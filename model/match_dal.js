var mysql   = require('mysql');
var db  = require('./db_connection.js');
var hero_picks_dal = require('./hero_picks_dal');
var Promise = require('promise');


var connection = mysql.createConnection(db.config);




exports.getBySetId = function(id, callback) {
    var query = "select * from match_ " +
        "where set_id = ?";
    var data = [id];

    var matches = [];

    connection.query(query, data, function(err, matchresult) {
        Promise.all(matchresult.map(function (match) {
            var promise = new Promise(function (resolve, reject) {
                hero_picks_dal.getById(match.team1_picks_id, function (err, result) {
                    //console.log("111", result);
                    var t1p = result;

                    hero_picks_dal.getById(match.team2_picks_id, function (err, result) {
                        var t2p = result;

                        resolve({
                            team1_picks: t1p,
                            team2_picks: t2p,
                            winner: match.winner
                        });
                    });
                });
            });
            return promise.then(function (result) {
                //console.log("p", result);
                matches.push(result);
            });
        })).then(function () {
            //console.log('matches', matches);
            callback(null, matches);
        });

    });
};



exports.insert = function(data, callback) {
    var query = "insert into match_ (set_id, team1_picks_id, team2_picks_id, winner) values (?, ?, ?, ?)";

    connection.query(query, data, function(err, results) {
        callback(err, results);
    });
};
