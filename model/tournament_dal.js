var mysql   = require('mysql');
var db  = require('./db_connection.js');

var connection = mysql.createConnection(db.config);

exports.getAll = function (callback) {
    var query = "select * from tournament";

    connection.query(query, function(err, result) {
        callback(err, result);
    });
}


exports.getById = function(id, callback) {
    var query = "select * from tournament " +
        "where tournament_id = ?";
    var data = [id];

    connection.query(query, data, function(err, result) {
        callback(err, result[0]);
    });
};
