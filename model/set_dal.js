var mysql   = require('mysql');
var db  = require('./db_connection.js');
var team_dal = require('./team_dal');
var match_dal = require('./match_dal');
var Promise = require('promise');

var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = "select * from set_";

    connection.query(query, function(err, result) {
        callback(err, result);
    })
};

exports.getLatestSets = function(callback) {
    var query = "select * from set_ s join match_ m on m.set_id = s.set_id group by s.set_id order by s.set_id desc limit 10";

    connection.query(query, function(err, setresult) {
        var sets = [];

        Promise.all(setresult.map(function(set) {
            var promise = new Promise(function(resolve, reject) {
                team_dal.getById(set.team1_id, function (err, result) {
                    var t1 = result.team_name;

                    team_dal.getById(set.team2_id, function (err, result) {
                        var t2 = result.team_name;

                        match_dal.getBySetId(set.set_id, function (err, result) {
                            var matches = result;
                            
                            var score = matches.map(function(m) {
                                return m.winner;
                            }).reduce(function(val, winner) {
                                if (winner == 1) {
                                    return val + 1;
                                }
                                else {
                                    return val - 1;
                                }
                            }, 0);

                            console.log(matches);
                            console.log("score", score);
                            resolve({
                                team1_name: t1,
                                team2_name: t2,
                                winner: score > 0 ? 1 : score < 0 ? 2 : 0,
                                matches: matches
                            });
                        });
                    });
                });
            });

            return promise.then(function (result) {
                //console.log("p", result);
                sets.push(result);
            });
        })).then(function () {
            //console.log('sets', sets);
            callback(null, sets);
        });

    });

};

exports.insert = function(params, callback) {
    var query = "insert into set_ (tournament_id, team1_id, team2_id) values (?, ?, ?)";
    var data = [params.tournament, params.team1, params.team2];

    connection.query(query, data, function(err, result) {
        callback(err, result);
    });
};




