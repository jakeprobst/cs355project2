var mysql   = require('mysql');
var db  = require('./db_connection.js');

var connection = mysql.createConnection(db.config);



exports.getById = function(id, callback) {
    var query = "select hero1, hero2, hero3, hero4, hero5 from team_picks " +
        "where team_picks_id = ?";
    var data = [id];

    connection.query(query, data, function(err, result) {
        //console.log('hp',result);
        var o = [result[0].hero1, result[0].hero2, result[0].hero3, result[0].hero4, result[0].hero5];
        callback(err, o);
    })
};

exports.insert = function(data, callback) {
    var query = "insert into team_picks (hero1, hero2, hero3, hero4, hero5) values (?, ?, ?, ?, ?)";

    connection.query(query, data, function(err, result) {
        //console.log(query, data, err, result);
        callback(err, result);
    });
};
