var express = require('express');
var router = express.Router();


var team_dal = require('../model/team_dal');
var tournament_dal = require('../model/tournament_dal');
var hero_picks_dal = require('../model/hero_picks_dal');
var match_dal = require('../model/match_dal');
var set_dal = require('../model/set_dal');


var herolist = [
    "Abaddon",
    "Alchemist",
    "Ancient Apparition",
    "Anti-Mage",
    "Arc Warden",
    "Axe",
    "Bane",
    "Batrider",
    "Beastmaster",
    "Bloodseeker",
    "Bounty Hunter",
    "Brewmaster",
    "Bristleback",
    "Broodmother",
    "Centaur Warrunner",
    "Chaos Knight",
    "Chen",
    "Clinkz",
    "Clockwerk",
    "Crystal Maiden",
    "Dark Seer",
    "Dazzle",
    "Death Prophet",
    "Disruptor",
    "Doom",
    "Dragon Knight",
    "Drow Ranger",
    "Earth Spirit",
    "Earthshaker",
    "Elder Titan",
    "Ember Spirit",
    "Enchantress",
    "Enigma",
    "Faceless Void",
    "Gyrocopter",
    "Huskar",
    "Invoker",
    "Io",
    "Jakiro",
    "Juggernaut",
    "Keeper of the Light",
    "Kunkka",
    "Legion Commander",
    "Leshrac",
    "Lich",
    "Lifestealer",
    "Lina",
    "Lion",
    "Lone Druid",
    "Luna",
    "Lycan",
    "Magnus",
    "Medusa",
    "Meepo",
    "Mirana",
    "Monkey King",
    "Morphling",
    "Naga Siren",
    "Nature's Prophet",
    "Necrophos",
    "Night Stalker",
    "Nyx Assassin",
    "Ogre Magi",
    "Omniknight",
    "Oracle",
    "Outworld Devourer",
    "Phantom Assassin",
    "Phantom Lancer",
    "Phoenix",
    "Puck",
    "Pudge",
    "Pugna",
    "Queen of Pain",
    "Razor",
    "Riki",
    "Rubick",
    "Sand King",
    "Shadow Demon",
    "Shadow Fiend",
    "Shadow Shaman",
    "Silencer",
    "Skywrath Mage",
    "Slardar",
    "Slark",
    "Sniper",
    "Spectre",
    "Spirit Breaker",
    "Storm Spirit",
    "Sven",
    "Techies",
    "Templar Assassin",
    "Terrorblade",
    "Tidehunter",
    "Timbersaw",
    "Tinker",
    "Tiny",
    "Treant Protector",
    "Troll Warlord",
    "Tusk",
    "Underlord",
    "Undying",
    "Ursa",
    "Vengeful Spirit",
    "Venomancer",
    "Viper",
    "Visage",
    "Warlock",
    "Weaver",
    "Windranger",
    "Winter Wyvern",
    "Witch Doctor",
    "Wraith King",
    "Zeus"];

router.get('/add', function(req, res) {
    team_dal.getAll(function(err, teamresult) {
        tournament_dal.getAll(function(err, tournamentresult) {
            res.render('set_add', {teams: teamresult, tournament: tournamentresult});
        });
    });
});

router.get('/add/match', function(req, res) {
    set_dal.insert(req.query, function(err, insertresult) {
        tournament_dal.getById(req.query.tournament, function(err, tournamentresult) {
            team_dal.getById(req.query.team1, function(err, team1result) {
                team_dal.getById(req.query.team2, function(err, team2result) {
                    res.render('match_add', {tournament: req.query.tournament,
                                             tournament_name: tournamentresult.tournament_name,
                                             team1id: req.query.team1,
                                             team1name: team1result.team_name,
                                             team2id: req.query.team2,
                                             team2name: team2result.team_name,
                                             herolist: herolist,
                                             set: insertresult.insertId});
                });
            });
        });
    });
});


router.get('/match/insert', function(req, res) {
    var team1picks = [req.query.team1hero1, req.query.team1hero2, req.query.team1hero3, req.query.team1hero4, req.query.team1hero5];
    var team2picks = [req.query.team2hero1, req.query.team2hero2, req.query.team2hero3, req.query.team2hero4, req.query.team2hero5];
    hero_picks_dal.insert(team1picks, function(err, team1result) {
        hero_picks_dal.insert(team2picks, function(err, team2result) {
            var matchdata = [req.query.set_id, team1result.insertId, team2result.insertId, req.query.winner];
            match_dal.insert(matchdata, function(err, matchresult) {
                 res.render('match_add', {tournament: req.query.tournament,
                                          tournament_name: req.query.tournament_name,
                                          team1id: req.query.team1id,
                                          team1name: req.query.team1name,
                                          team2id: req.query.team2id,
                                          team2name: req.query.team2name,
                                          herolist: herolist,
                                          set: req.query.set_id,
                                          saved: true});
            });
        });
    });
});



module.exports = router;
