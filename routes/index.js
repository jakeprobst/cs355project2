var express = require('express');
var router = express.Router();

var set_dal = require('../model/set_dal');


router.get('/', function(req, res, next) {
  set_dal.getLatestSets(function(err, result) {
      console.log(err, result);
    if (err) {
      res.send(err);
    }
    else {
      console.log(result);
      res.render('index', {sets: result});
    }
  });
});

module.exports = router;
